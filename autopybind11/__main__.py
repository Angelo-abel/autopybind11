# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import copy
import fileinput
import glob
import json
import logging
import os
import re
import posixpath
import re

from configargparse import ArgParser, YAMLConfigFileParser
import pygccxml
import pygccxml.declarations as dec
import yaml

from autopybind11.bindings_sorter import get_binding_order
from autopybind11.op_names import names_dict, arg_dependent_ops
import autopybind11.text_blocks as tb
import autopybind11.customizer as customizer
from autopybind11._formatting import maybe_format_file_in_place


class BindingsGenerator:
    def __init__(self, opts, starting_indent=""):
        self.opts = opts
        self.indent = starting_indent
        self.name_data = {}
        self.data_web = {}
        self.name_tree = {}
        self.name_tree[self.opts.module_name] = {}
        self.skip_list = set()
        # Dictionary to maintain a namespace -> auto_bind pointer for sub namespaces
        self.namespace_web = {}
        self.customizer = customizer.Customizer(self.name_tree[self.opts.module_name])

    def load_mdx(self, file_name):
        with open(file_name, "r") as mdx_file:
            mdx_data = json.load(mdx_file)
            for full_name, auto_data_fnc in mdx_data.items():
                if full_name in self.data_web:
                    print(
                        "Error: attempting to capture two objects with the same name."
                    )
                    print(
                        "%s from %s and %s from %s"
                        % (
                            full_name,
                            auto_data_fnc,
                            full_name,
                            self.data_web[full_name],
                        )
                    )
                self.data_web[full_name] = auto_data_fnc

    def flatten(self, d_in, flat):
        for key in d_in.keys():
            flat.add(key)
            if type(d_in[key]) is dict:
                self.flatten(d_in[key], flat)

    def write_data_to_file(self, file_name, string):
        """
        Writes incoming strings to an output file, globally keep track of objects written?
        :param file_name: name of file to write to
        :param string: string to write out
        :return: None
        """
        assert file_name.endswith(".cpp"), cpp_file
        with open(file_name, "w") as cpp_file:
            cpp_file.write(string)
        maybe_format_file_in_place(file_name)

    def get_include_str(self, found_includes):
        include_str = ""
        for f in found_includes:
            include_str += '#include "%s"\n' % f
        return include_str

    def generate_enum_string(
        self, pybind_module_name, declaration, cpp_enum, enum_type=""
    ):
        cust_name = self.customizer.get_custom_name(cpp_enum)

        val_str = self.opts.enum_header_fmt.format(
            class_name=declaration.decl_string,
            module=pybind_module_name,
            name=declaration.name if cust_name is None else cust_name,
            type=enum_type if enum_type else "py::arithmetic()",
            doc="",
        )
        # Check if YAML has custom names for enumerations
        custom_listing = self.customizer.get_custom_enum_vals(cpp_enum)

        for enum_obj in declaration.get_name2value_dict().keys():
            scope_name = enum_obj
            if declaration.decl_string != "::":
                scope_name = "%s::%s" % (declaration.decl_string, enum_obj)
            if enum_obj in custom_listing:
                enum_obj = custom_listing[enum_obj]
            val_str += self.opts.enum_val_fmt.format(
                short_name=enum_obj, scoped_name=scope_name, doc=""
            )
        val_str += ".export_values();\n"
        return val_str

    def check_protected_return_class(self, fn):
        match = re.search(
            r"::(?P<cls_name>\w+?)( |$)", fn.return_type.decl_string
        )
        if match:
            class_name = match.group(1)
            for found_obj in self.name_data.classes(
                class_name, recursive=True
            ):
                if self.protected_filter(found_obj):
                    return True
        return False

    def generate_function_string(
        self,
        member_function,
        is_free_function=False,
        module_name="m",
        py_name="",
        publicist_name="",
    ):
        """
        Takes a set of PyGCCXML data and returns the PyBind11 code for
        that function

        :param member_function: PyGCCXML data dictionary for individual function
        :param is_free_function: Boolean to determine if function should be marked
        as a member of the PyBind11 module or of a class. Essentially, prepends "m" to function signature
        is_free_function = True
          m.def("ExtractDoubleOrThrow", py::overload_cast<double const &>(ExtractDoubleOrThrow),<...>
        is_free_function = False
          .def("start_time", py::overload_cast<>(start_time),, , doc.PiecewiseTrajectory.start_time.doc)
        :param py_name: Name of the function on the python side. Defaults to the same
        name as the C++ function
        :param module_name: Name of the pybind11 module to apply free function to, default to "m". Will be set
        as submodules are created.
        :return: A string which contains all PyBind11 declarations for single function
        """

        # If a different name is requested on the Python side, set it here
        fun_name = py_name if py_name else member_function.name

        # Capture each argument and a default value, if found.
        arg_string = ""

        for arg in member_function.arguments:
            default_val = ""
            if arg.default_value:
                fmt_string = self.opts.arg_val_cast_fmt
                if (
                    " " in arg.decl_type.decl_string
                    and not arg.default_value.startswith("{")
                ):
                    fmt_string = self.opts.nullptr_arg_val_fmt
                default_val = fmt_string.format(
                    type=arg.decl_type.decl_string.replace(
                        "const &", ""
                    ).strip(),
                    val=arg.default_value,
                )
            next_arg_str = self.opts.member_func_arg_fmt.format(
                arg.name, default_val
            )
            arg_string = arg_string + ", " + next_arg_str

        if member_function.parent.name == "::":
            ref_string = "&%s" % (member_function.name)

        else:
            parent = ""
            if not is_free_function and self.protected_filter(member_function):
                if publicist_name:
                    parent = publicist_name
                else:
                    msg = (
                        "No publicist name set for protected virtual method %s"
                        % member_function.name
                    )
                    raise RuntimeError(msg)
            else:
                parent = member_function.parent.decl_string
            ref_string = "&%s::%s" % (parent, member_function.name)
        # Check to see if a return class might be protected.
        decl_string = member_function.decl_string
        if self.check_protected_return_class(member_function):
            # Replace the first instance of the parent's declstring
            # It should replace the return type only.
            decl_string = decl_string.replace(
                member_function.parent.decl_string,
                self.template_args_to_underscores(member_function.parent.name)
                + "_publicist",
                1,
            )
        signature = self.opts.overload_template_fmt.format(
            fun_ref=ref_string, decl_string=decl_string
        )

        # Check to see if function can be marked as static
        static = ""
        if "has_static" in dir(member_function):
            static = "_static" if member_function.has_static else ""

        # Return formatted function string
        member_string = self.opts.member_func_fmt.format(
            module=module_name if is_free_function else "",
            static=static,
            fun_name=fun_name,
            fun_ref=signature,
            args=arg_string,
            classname_doc=member_function.parent.decl_string,
            doc=", doc.%s.%s.doc"
            % (member_function.parent.name.split("<")[0], member_function.name)
            if False
            else "",
            ending=";" if is_free_function else "",
        )
        member_string.strip(",")

        return member_string

    def find_getter(self, var_data, class_data):
        name_to_find = "get_" + var_data.name
        if name_to_find in [fxn.name for fxn in class_data.member_functions()]:
            return name_to_find

        return ""

    def find_setter(self, var_data, class_data):
        name_to_find = "set_" + var_data.name
        if name_to_find in [fxn.name for fxn in class_data.member_functions()]:
            return name_to_find

        return ""

    def generate_operator_string(self, oper_data, is_member_fxn=True):
        """
        Accepts a pygccxml data object which represents a marked operator for a
        class.  Check for overloads and generate a valid binding code as a string to
        be returned
        :param oper_data: A pygccxml object representing a variable
        :return: a string containing the PyBind11 declaration for the operator
        """
        if oper_data.name == "operator=":
            return ""

        num_args = len(oper_data.arguments)
        symbol = oper_data.symbol
        py_name = ""

        # Some C++ operators have multiple python names depending on
        # the number of arguments. We'll check which py_name to use here
        if symbol in arg_dependent_ops:
            is_unary = (
                is_member_fxn
                and num_args == 0
                or not is_member_fxn
                and num_args == 1
            )

            if is_unary:
                py_name = names_dict[symbol][0]

            else:
                py_name = names_dict[symbol][1]

        else:
            py_name = names_dict[symbol]

        if not py_name:
            raise RuntimeError("py_name not set")

        return self.generate_function_string(oper_data, py_name=py_name)

    def generate_member_var_string(self, var_data, written_functions):
        """
        Accepts a pygccxml object representing a variable.
        Checks whether the variable is writeable and static,
        then generates valid binding code for that variable as a string.

        :param var_data: a PyGCCXML object representing a variable
        :param written_functions: Growing list of bound functions generated
        This is necessary since, if pm_flag is true, we will start binding getters and setters here.
        We don't want to duplicate them when we bind the member functions, so we'll mark them here
        :return: a string containing the PyBind11 declaration for the member variable
        """

        var_name_str = var_data.name
        classname_str = var_data.parent.decl_string

        is_public = var_data.access_type == dec.ACCESS_TYPES.PUBLIC

        if is_public:
            # Check if the variable is writeable
            is_const = dec.is_const(
                dec.remove_volatile(dec.remove_reference(var_data.decl_type))
            )
            writeable_str = "write" if not is_const else "only"

            # Get a string representing a reference to the variable
            ref = self.opts.member_reference_fmt.format(
                classname=classname_str, member=var_name_str
            )

            # Check if it is static
            is_static = var_data.type_qualifiers.has_static
            static_str = "_static" if is_static else ""

            return self.opts.public_member_var_fmt.format(
                write=writeable_str,
                static=static_str,
                var_name=var_name_str,
                var_ref=ref,
            )
        elif self.opts.pm_flag:
            # First find if there is a corresponding getter/setter
            getter_fxn_name = self.find_getter(var_data, var_data.parent)
            setter_fxn_name = self.find_setter(var_data, var_data.parent)

            # If neither could be found, return empty str
            if not getter_fxn_name and not setter_fxn_name:
                return ""

            # At this point, at least one of the functions was found,
            # so we can start generating the string
            accessors_string = ""
            is_readonly = True

            # Add the getters and setters
            if getter_fxn_name:
                ref = self.opts.member_reference_fmt.format(
                    classname=classname_str, member=getter_fxn_name
                )
                accessors_string += ref
                written_functions.append(getter_fxn_name)

            if setter_fxn_name:
                ref = self.opts.member_reference_fmt.format(
                    classname=classname_str, member=setter_fxn_name
                )
                separator = ", " if accessors_string else ""
                accessors_string += separator + ref
                written_functions.append(setter_fxn_name)

                # Also change the status of is_readonly
                is_readonly = False

            readonly_str = "_readonly" if is_readonly else ""

            is_static = var_data.type_qualifiers.has_static
            static_str = "_static" if is_static else ""

            return self.opts.private_member_var_fmt.format(
                readonly=readonly_str,
                static=static_str,
                var_name=var_name_str,
                var_accessors=accessors_string,
            )

        # If the variable isn't public, and the option to
        # expose the variable through getters and setters is False,
        # the variable won't be directly accessible from python
        # Getters and setters may still be bound when the member fxns are written
        return ""

    # Takes a list of function data, turns it into a long string of data
    # writes that string to a named file.
    # TODO: Documentation
    def write_non_class_data(
        self,
        module_name,
        function_data,
        enum_data,
        out_dir,
        found_includes,
        fun_yaml={},
        enum_yaml={},
    ):
        """
        Takes a list of function objects and writes them out to a PyBind11 module.
        The module is named for the first argument and the file is written to the out_dir


        :param module_name: String name of grouping to be used as PyBind11 module name
        :param function_data: List of PyGCCXML objects which describe functions
        :param out_dir: File location to store the resultant file.
        :param found_includes:
        :param fun_yaml: Yaml information parsed from wrapper_input about given free functions
        :param enum_yaml: Yaml information parsed from wrapper_input about given enums
        :return: None
        """
        self.indent += " " * 2
        keys = {
            "includes": self.get_include_str(found_includes),
            "namespace": module_name + "_py",
            "defs": "",
            "forwards": "",
            "module": self.opts.module_name,
            "name": module_name,
        }
        cpp_body = self.opts.non_class_module_cpp_fmt
        pybind_module_name = "m"
        if module_name != "free_functions" and not self.opts.ins_flag:
            pybind_module_name = module_name
        for function in function_data:
            cpp_name = function.name
            cpp_fun = fun_yaml[cpp_name]
            cust_name = self.customizer.get_custom_name(cpp_fun)
            keys["defs"] += self.generate_function_string(
                function,
                py_name="" if cust_name is None else cust_name,
                module_name=pybind_module_name,
                is_free_function=True,
            )
            keys["defs"] += self.indent

        # TODO: Need to find other enum types to use as examples.
        for declaration in enum_data:
            cpp_name = declaration.name
            cpp_enum = enum_yaml[cpp_name]
            if declaration.decl_string == "::":
                continue
            keys["defs"] += self.generate_enum_string(
                pybind_module_name, declaration, cpp_enum
            )
        keys["defs"] = keys["defs"].strip()

        file_name = posixpath.join(out_dir, "%s_py.cpp" % module_name)
        if module_name != "free_functions" and not self.opts.ins_flag:
            cpp_body = self.opts.non_class_module_return_cpp_fmt
            submodules_str = (
                self.opts.submodule_signature_fmt.format(name=module_name)
                + "\n"
                + self.indent
            )

            keys["defs"] = submodules_str + keys["defs"]

        self.write_data_to_file(
            file_name, cpp_body.format(**keys)
        )
        self.indent = self.indent[:-2]

    def all_virt_filter(self, x):
        return x.virtuality != "not virtual"

    def pure_virt_filter(self, x):
        return x.virtuality == "pure virtual"

    def non_pure_virt_filter(self, x):
        return x.virtuality == "virtual"

    def public_filter(self, x):
        return x.access_type == "public"

    def protected_filter(self, x):
        # Checking classes for protected, access_type
        # is further into the object.
        if "cache" in dir(x):
            return x.cache.access_type == "protected"
        return x.access_type == "protected"

    def private_filter(self, x):
        return x.access_type == "private"

    # If the method is private and non_pure virtual, we will not add it to
    # the trampoline, as doing so would result in a compiler error.
    def virt_method_supported(self, x):
        return self.all_virt_filter(x) and not (
            self.private_filter(x) and self.non_pure_virt_filter(x)
        )

    def remove_classname_from_method(self, fun):
        fun_str = str(fun)

        # First, we have to remove the [member_function] part
        # return_type nmspc1::Base::foo(args) [member_function] ->
        # return_type nmspc1::Base::foo(args)
        fun_str = fun_str.replace(" [member function]", "")

        # Next we have to remove the current class name
        # return_type nmspc1::Base::foo(args) ->
        # return_type foo(args)
        to_be_replaced = fun.parent.decl_string.strip("::") + "::" + fun.name
        return fun_str.replace(to_be_replaced, fun.name)

    def get_tramp_overload_macro_args(self, class_inst, alias, fun):
        keys = {
            "return_type": fun.return_type,
            "parent_alias": alias,
            "cpp_fxn_name": fun.name,
            "arg_str": "",
        }

        # Go through and construct argument string
        self.indent += " " * 2
        for i, arg in enumerate(fun.arguments):
            if i:
                keys["arg_str"] += ",\n" + self.indent
            keys["arg_str"] += arg.name

        self.indent = self.indent[:-2]

        return self.opts.pybind_overload_macro_args_fmt.format(**keys).strip()

    def find_tramp_methods(self, class_inst):
        # tramp_methods holds the methods to return
        # We'll use the signatures of the functions (with the classname removed) to check
        # whether this method needs to be overriden in the trampoline. These are stored in
        # sigs_to_skip. It could be the case that a base and derived class have methods
        # with the same name, but different return/arg types or # of args.
        # This will make sure we don't miss any.
        tramp_methods = list()
        sigs_to_skip = set()

        virt_methods = [
            f for f in class_inst.member_functions(self.all_virt_filter)
        ]
        for m in virt_methods:
            if self.virt_method_supported(m):
                tramp_methods.append(m)

            sigs_to_skip.add(self.remove_classname_from_method(m))

        # Now we'll loop through all of the bases
        # and add any missing methods that were inherited
        for hierarchy in class_inst.recursive_bases:
            base = hierarchy.related_class
            for m in base.member_functions(self.all_virt_filter):
                final_flag = True
                if m.attributes:
                    if "final" in m.attributes:
                        final_skip = False

                stripped_sig = self.remove_classname_from_method(m)
                if stripped_sig not in sigs_to_skip:
                    if self.virt_method_supported(m) and final_flag:
                        tramp_methods.append(m)

                    sigs_to_skip.add(stripped_sig)

        return tramp_methods

    def get_tramp_overrides(self, class_inst, alias, tramp_methods):
        overrides_acc = ""
        published_names = set()
        for m in tramp_methods:
            if m.name in published_names:
                continue
            published_names.add(m.name)
            keys = dict()

            keys["fxn_sig"] = self.remove_classname_from_method(m)
            keys["pure"] = "_PURE" if self.pure_virt_filter(m) else ""

            self.indent += " " * 2
            keys["macro_args"] = self.get_tramp_overload_macro_args(
                class_inst, alias, m
            )
            self.indent = self.indent[:-2]
            keys["return_type"] = m.return_type
            overrides_acc += self.opts.tramp_override_fmt.format(**keys)
            overrides_acc += "\n" * 2 + self.indent

        return overrides_acc.strip()

    def get_trampoline_string(
        self, class_inst, cpp_class_name, tramp_name, methods
    ):
        """
        Assumes that the instance has at least 1 virtual method
        """
        alias = cpp_class_name + "_alias"
        keys = dict()
        keys["tramp_name"] = tramp_name
        keys["class_decl"] = class_inst.decl_string
        keys["parent_alias"] = alias
        keys["ctor_name"] = cpp_class_name
        keys["virtual_overrides"] = self.get_tramp_overrides(
            class_inst, alias, methods
        )

        return self.opts.trampoline_def_fmt.format(**keys).strip()

    def get_publicist_using_directives(self, class_inst, methods):
        directives = ""
        for m in methods:
            keys = dict()
            keys["class_decl"] = class_inst.decl_string
            keys["fxn_name"] = m.name

            directives += self.opts.publicist_using_directives_fmt.format(
                **keys
            )
            directives += "\n" + self.indent

        return directives.strip()

    def get_publicist_string(self, class_inst, publicist_name, methods):
        keys = dict()
        keys["publicist_name"] = publicist_name
        keys["class_decl"] = class_inst.decl_string
        keys["using_directives"] = self.get_publicist_using_directives(
            class_inst, methods
        )

        return self.opts.publicist_def_fmt.format(**keys).strip()

    def sanitized_string(self, input_string):
        """
        Ensure input to eval is safe
        :param input_string: string to be check and sanitized
        :return: boolean indicating safety
        """
        # regex check for more than one lambda function (no nesting lambdas)
        lambda_list = re.findall("lambda", input_string)
        if len(lambda_list) > 1:
            return False
        double_under = re.findall("__.*__", input_string)
        if double_under:
            return False
        blacklist = re.findall("[*.()\n;/]", input_string)
        if blacklist:
            return False
        return True

    # TODO: Handle nested template types
    def get_templated_type(
        self, pyclass_name_stem, class_data, cust_name, idx
    ):
        """
        Extract templated type from class dec regex <.+?>

        :param pyclass_name_stem: default class name generated by pygccxml
        :param class_data: A yaml dict of class data read in from wrapper_input under a 'class' header
        :return: customized class name string based on cust_name in class_data
        """
        # Get custom name string - lambda or string
        if "inst" in class_data and class_data["inst"]:
            if type(cust_name) is not list:
                if not self.sanitized_string(cust_name):
                    raise RuntimeError(
                        "Unacceptable custom name: " + cust_name
                    )
                cust_name_gen = eval(cust_name, {"__builtins__": {}}, {})
                # Lambda function
                # Evaluate yaml custom name input string, either lambda function or list of custom names
                t_type = re.search("<.+?>", pyclass_name_stem).group()
                # Handle double templates, by breaking into a list
                t_type = t_type.strip("<").strip(">").split(",")
                pyclass_name = cust_name_gen(t_type)
            else:
                pyclass_name = cust_name[idx]
        else:
            pyclass_name = cust_name
        return pyclass_name

    def write_class_data(
        self,
        cpp_class_name,
        instance_list,
        out_dir,
        found_includes,
        desired_name,
        class_data,
    ):
        """
        Takes an instance of Class data from PyGCCXML and outputs a single file with
        PyBind11 declarations for the class. Includes constructors and functions

        :param cpp_class_name: Name of the class without any template args
        :param instance_list: A list of pygccxml data dictionaries. One dictionary per class instance
        :param out_dir: File location to store the resultant file.
        :param found_includes: List of files to include in the wrapping code
        :param desired_name: String value for a typedef-ed object to set the typedef name as the name for the module
        :param class_data: A yaml Python dict of class data read in from wrapper_input under a 'class' header
        :return: None
        """
        skip_write_flag = False
        if cpp_class_name in self.skip_list:
            skip_write_flag = True
        # Increase the indent
        self.indent += " " * 4
        newlines = "\n" * 2
        keys = {
            "includes": self.get_include_str(found_includes),
            "trampoline_str": "",
            "publicist_str": "",
            "namespace": cpp_class_name + "_py",
            "defs": "",
            "module": self.opts.module_name,
            "forwards": set(),
        }
        file_name = posixpath.join(out_dir, cpp_class_name + "_py.cpp")
        class_skip_list = self.customizer.get_blacklisted_members(class_data)

        for idx, instance_data in enumerate(instance_list):
            dep_call_str = ""
            keys["defs"] += newlines

            pyclass_name = self.template_args_to_underscores(
                instance_data.name
            )

            # If module specific name was found here, use it instead of the above name.
            # This means somewhere else there is a class with the same name.
            if (
                "%s_%s" % (self.opts.module_name, instance_data.name)
                in self.data_web
            ):
                pyclass_name = "%s_%s" % (
                    self.opts.module_name,
                    cpp_class_name,
                )
            if not desired_name == "":
                pyclass_name = desired_name
            # If user defines custom class name, use instead of default, see 'else'
            cust_name = self.customizer.get_custom_name(class_data)
            if cust_name is not None:
                # Get custom name string, potentially with type notations inserted
                pyclass_name = self.get_templated_type(
                    instance_data.name, class_data, cust_name, idx
                )

            # Get the arguments to the py::class_<>() call
            # The first of which is the class name
            pyclass_args = instance_data.decl_string

            # Next any super classes
            # if desired name added as guard
            # Essentially, prevents typedefs from adding parent classes.
            # TODO: Find a better way to eliminate parent classes when necessary
            if desired_name == "":
                for b in instance_data.bases:
                    # If the relationship is public, add to pyclass_args
                    if b.access_type == dec.ACCESS_TYPES.PUBLIC:
                        if b.related_class.name in self.skip_list:
                            skip_write_flag = True
                        pyclass_args += ", " + b.related_class.decl_string

            tramp_methods = self.find_tramp_methods(instance_data)
            # If there exist any virtual functions abiding by certain criteria
            # (see find_tramp_methods), we'll need to write out a trampoline implementation
            tramp_name = ""
            tramp_methods = [
                x for x in tramp_methods if x.name not in class_skip_list
            ]
            if (
                tramp_methods
                and dec.type_traits_classes.has_trivial_constructor(
                    instance_data
                )
            ):
                self.indent = self.indent[:-2]
                tramp_name = pyclass_name + "_trampoline"
                tramp_str = self.get_trampoline_string(
                    instance_data, cpp_class_name, tramp_name, tramp_methods
                )
                keys["trampoline_str"] += tramp_str
                keys["trampoline_str"] += newlines
                self.indent += " " * 2

                # Also need to add to pyclass_args
                pyclass_args += ", " + tramp_name

            # Now we'll deal with any protected functions, as we want these to be visible
            # to python subclasses. Especially if they are virtual and appear in the
            # trampoline for overriding.
            prot_methods = [
                x for x in list(
                    instance_data.member_functions(self.protected_filter)
                ) if x.name not in class_skip_list
            ]
            prot_classes = list(instance_data.classes(self.protected_filter))
            total_protected = prot_classes + prot_methods
            publicist_name = ""
            if total_protected:
                self.indent = self.indent[:-2]
                publicist_name = pyclass_name + "_publicist"
                publicist_str = self.get_publicist_string(
                    instance_data, publicist_name, total_protected
                )
                keys["publicist_str"] += publicist_str
                keys["publicist_str"] += newlines
                self.indent += " " * 2

            constructor_str = ""
            # List to stuff names into which will prevent re-writing
            written_functions = []

            if not instance_data.is_abstract:
                for constructorObj in instance_data.constructors(
                    self.public_filter
                ):
                    arg_string = ""
                    arg_name_string = ","

                    for arg in constructorObj.arguments:
                        arg_string += arg.decl_type.decl_string + ","
                        default_val = ""
                        if arg.default_value:
                            fmt_string = self.opts.arg_val_cast_fmt
                            # startswith is used to prevent a (<type>){val, val, val} casting string
                            # which is a non-standard explicit type conversion syntax error.
                            if (
                                " " in arg.decl_type.decl_string
                                and not arg.default_value.startswith("{")
                            ):
                                fmt_string = self.opts.nullptr_arg_val_fmt
                            default_val = fmt_string.format(
                                type=arg.decl_type.decl_string.replace(
                                    "const &", ""
                                ).strip(),
                                val=arg.default_value,
                            )
                        arg_name_string += self.opts.member_func_arg_fmt.format(
                            arg.name, default_val
                        )
                        arg_name_string += ","
                    arg_string = arg_string.rstrip(",")
                    arg_name_string = arg_name_string.rstrip(",")

                    # If we're using a trampoline, we need to be careful of
                    # the copy constructor (CC).
                    if tramp_methods and dec.is_copy_constructor(
                        constructorObj
                    ):
                        cname = instance_data.decl_string
                        cc_keys = {
                            "arg_type": arg_string,
                            "cname": cname,
                            "doc": "",
                            "indent": self.indent,
                            "tramp_arg_type": arg_string.replace(
                                cname, tramp_name
                            ),
                            "trampname": tramp_name,
                        }
                        constructor_str += self.opts.copy_constructor_tramp_fmt.format(
                            **cc_keys
                        )

                    else:
                        constructor_str += self.opts.constructor_fmt.format(
                            arg_string, arg_name_string, ", " if False else ""
                        )

                    constructor_str += self.indent

            # If virtual functions are present and the class is abstract,
            # we actually have to utilize the default constructor for the trampoline
            elif (
                tramp_methods
                and dec.type_traits_classes.has_trivial_constructor(
                    instance_data
                )
            ):
                # No need to look for arguments
                constructor_str += self.opts.constructor_fmt.format(
                    "", "", ", " if False else ""
                )

            member_var_string = ""
            for member_var in instance_data.variables():
                if (
                    member_var.name in class_skip_list
                ):
                    continue
                member_var_string += self.generate_member_var_string(
                    member_var, written_functions
                )
                member_var_string += self.indent

            member_string = ""
            for member_function in instance_data.member_functions():
                if (
                    member_function.name in written_functions
                    or self.private_filter(member_function)
                    or member_function.name in class_skip_list
                ):
                    continue
                member_string += self.generate_function_string(
                    member_function, publicist_name=publicist_name
                )
                member_string += self.indent

            # TODO: Necessary?  Determine usefulness of listing operators
            operator_string = ""
            for operator in instance_data.operators():
                operator_string += self.generate_operator_string(operator)
                operator_string += self.indent
            no_delete_string = ""
            if not dec.type_traits_classes.has_public_destructor(
                instance_data
            ):
                no_delete_string = (
                    ", std::unique_ptr<%s, py::nodelete>"
                    % instance_data.decl_string
                )
            enum_string = ""
            for enum in instance_data.enumerations():
                enum_string += self.generate_enum_string(
                    pyclass_name, enum, class_data
                )
            py_module_local = ""
            is_mod_local = self.customizer.get_module_local_value(class_data)
            if is_mod_local:
                py_module_local = self.opts.module_local_fmt
            keys["defs"] += self.opts.class_info_body_fmt.format(
                name=pyclass_name,
                pyclass_args=pyclass_args,
                doc=", doc.%s.doc)" % instance_data.name if False else "",
                mod_loc=py_module_local,
                dependency_calls=dep_call_str,
                no_delete=no_delete_string,
                constructor=constructor_str.strip(),
                funcs=member_string.strip(),
                vars=member_var_string.strip(),
                opers=operator_string.strip(),
                enums=enum_string.strip(),
            )
        keys["defs"] = keys["defs"].strip()
        keys["forwards"] = "".join(keys["forwards"])

        if skip_write_flag:
            # Empty the value here, so we don't miss the file, but write no data about a class
            # that is missing some wrapping information.
            print(
                "Warning: Class %s will not have wrapping generated due to missing information."
                % cpp_class_name
            )
            print("Missing data: %s" % self.skip_list)
            keys["defs"] = ""
            keys["forwards"] = ""
        self.write_data_to_file(
            file_name, self.opts.class_module_cpp_fmt.format(**keys)
        )
        self.indent = self.indent[:-4]

    def build_namespace_structure(self, namespace_s, line_out, prev_nmspc):
        for key in namespace_s.keys():
            sub_name = key+"_py"
            line_out["forwards"].append(
                self.opts.init_fun_forward_non_void_fmt.format(
                    mod_name=key, name=sub_name, module=self.opts.module_name,
                )
            )
            # TODO change the init_fun_signature to include diff mod name (module)
            mod_var = prev_nmspc if not prev_nmspc == self.opts.module_name\
                else "model"
            line_out["init_funs"].append(
                self.opts.submodule_init_fun_signature_return_fmt.format(
                    mod_name=key,
                    name=sub_name,
                    module=self.opts.module_name,
                    module_var=mod_var,
                )
            )
            self.build_namespace_structure(namespace_s[key], line_out, key)

    def write_module_data(self, module_name, results_dict, out_dir):
        """
        Writes out the "folder" level module for wrapping.
        THis file follows
        :param module_name:
        :param results_dict:
        :param out_dir:
        :return: The name of the module file to include in the library
        """

        module_data = {"forwards": [], "init_funs": []}
        no_forward_list = set()
        nmspcs = set()
        if not self.opts.ins_flag:
            self.flatten(self.name_tree, nmspcs)
            self.build_namespace_structure(self.name_tree[self.opts.module_name],
                                           module_data,
                                           self.opts.module_name)

        # TODO this may need to be an insert
        if self.opts.top_module:
            module_data["init_funs"].append(
                self.opts.declare_parent_module.format(
                    parent=self.opts.top_module,
                    name=self.opts.module_name,
                )
            )
        for future_file in results_dict["out_names"]:
            name = future_file.split(".")[0]
            # we define the namespace structure in above call to build_namespc_str
            # so we only need the classes and free funcs here
            if name.split('_py')[0] not in nmspcs:
                mod = results_dict["modules"][future_file]
                sub_mod = mod if not mod == self.opts.module_name\
                    and bool(mod) else "model"
                module_data["forwards"].append(
                    self.opts.init_fun_forward_fmt.format(
                        name=name, module=self.opts.module_name,
                    )
                )
                module_data["init_funs"].append(
                    self.opts.submodule_init_fun_signature_fmt.format(
                        name=name, module=self.opts.module_name, module_var=sub_mod
                    )
                )
        module_cpp_file = posixpath.join(out_dir, "%s.cpp" % module_name)
        with open(module_cpp_file, "w") as module_file:
            module_file.write(
                self.opts.common_cpp_body_fmt.format(
                    name=module_name,
                    forwards="".join(
                        module_data["forwards"]
                        + list(results_dict["all_auto_bind_defs"])
                    ),
                    init_funs="".join(module_data["init_funs"]),
                    autobind_calls="".join(results_dict["all_auto_bind"]),
                )
            )
        maybe_format_file_in_place(module_cpp_file)
        return module_cpp_file

    def populate_namespace_web(self, mod_name):
        self.namespace_web[mod_name] = {"mod_name": "",
                                        "all_gen_fun_data": list(),
                                        "all_gen_enum_data": list(),
                                        "files_to_include": set(),
                                        "funs_dict": {},
                                        "enums_dict": {}
                                        }

    # Adds the namespace to the function or class name
    # Can pass in "" as namespace
    def add_namespace(self, namespace, name):
        if namespace:
            return namespace + "::" + name
        else:
            return name

    def find_future_file_name(
        self, is_class, name, free_fun_name="", curr_nmspc=""
    ):
        ret = ""
        suf = "_py.cpp"
        if is_class:
            ret = name.split("<")[0]
        elif curr_nmspc:
            # Only use most recent namespace if nested
            ret = self.most_recent_namespace(curr_nmspc)
        else:  # free function in no namespace
            ret = free_fun_name
        ret += suf
        return ret

    def find_module_data(
        self, yaml_dict, res_dict, free_fun_name, curr_nmspc="", mod_tree={}
    ):
        auto_bind_func = "apb11_" + self.opts.module_name
        curr_mod = (
            self.most_recent_namespace(curr_nmspc) if curr_nmspc else self.opts.module_name
        )
        if not curr_nmspc:
            if not self.opts.cns_flag:
                mod_tree = self.name_tree
            self.populate_namespace_web(curr_mod)
        for key in yaml_dict:
            if key in ["classes", "functions", "enums"]:
                is_class = key == "classes"
                inner_dict = yaml_dict[key]
                for name, data in inner_dict.items():
                    # First add the dependent file
                    res_dict["to_include"].add(data["file"])
                    # Then write the future file
                    future_file = self.find_future_file_name(
                        is_class, name, free_fun_name, curr_nmspc
                    )
                    custom_mod_name = self.customizer.get_add_custom_namespace(
                                                    data,
                                                    )
                    if custom_mod_name:
                        self.populate_namespace_web(custom_mod_name)
                        cust_nmspc_future_file = self.find_future_file_name(
                            is_class, custom_mod_name, free_fun_name, custom_mod_name
                        )
                        res_dict["out_names"][cust_nmspc_future_file] = ""
                        if is_class:
                            res_dict["out_names"][future_file] = custom_mod_name
                    else:
                        ff_name = future_file
                        obj_to_nmspc = ""
                        if self.opts.cns_flag:
                            if not is_class:
                                ff_name = free_fun_name + "_py.cpp"
                        elif not self.opts.ins_flag:
                            obj_to_nmspc = curr_mod
                        res_dict["out_names"][ff_name] = obj_to_nmspc
                    if "inst" in data and data["inst"]:
                        # TODO. Logic of next 3 lines will need to change when
                        # support for typed enums is added
                        if key == "enums":
                            raise RuntimeError(
                                "Typed enums not currently supported"
                            )
                        key_to_inst_list = (
                            "class_insts" if is_class else "func_insts"
                        )
                        all_inst_names = self.get_all_inst_names(name, data)
                        all_inst_names = [
                            self.add_namespace(curr_nmspc, inst)
                            for inst in all_inst_names
                        ]
                        for inst in all_inst_names:
                            self.data_web[inst] = auto_bind_func

                        res_dict[key_to_inst_list].extend(all_inst_names)
                    elif is_class:
                        res_dict["non_template_classes"].append(name)
                        # Use add_namespace to match templated classes
                        self.data_web[
                            self.add_namespace(curr_nmspc, name)
                        ] = auto_bind_func
            else:  # Found a namespace. Recurse!
                new_nmspc = self.add_namespace(curr_nmspc, key)

                # Add new namespace to res_dict, rather than current
                # prevents functionless namespaces from being dropped
                # also will allow us to handle empty namespaces
                if not self.opts.ins_flag:
                    future_file = self.find_future_file_name(
                        False, new_nmspc, free_fun_name, new_nmspc
                    )
                    if not self.opts.cns_flag:
                        res_dict["out_names"][future_file] = curr_mod
                        mod_tree[curr_mod][key] = {}

                    if curr_nmspc != "":
                        # Use add_namespace to match templated classes
                        self.data_web[
                            self.add_namespace(curr_nmspc, key)
                        ] = auto_bind_func
                mod_tree_itr = {}
                if not self.opts.cns_flag:
                    self.populate_namespace_web(key)
                    mod_tree_itr = mod_tree[curr_mod]
                self.find_module_data(
                    yaml_dict[key], res_dict, free_fun_name, new_nmspc, mod_tree_itr
                )

    def clean_flags(self, rsp_path):
        rsp_includes = []
        rsp_indx_files = []
        rsp_defs = ""
        c_std_flag = ""

        # No path specified? Return 0 flags
        if not rsp_path:
            return rsp_includes, rsp_defs, rsp_indx_files

        with open(rsp_path, "r") as fp:
            for line in fp.readlines():
                line = line.strip().replace(";", " ").split(" ")
                if line[0] == "includes:":
                    rsp_includes = line[1:]

                elif line[0] == "defines:":
                    rsp_defs = " ".join(
                        ["-D" + def_ if def_ else "" for def_ in line[1:]]
                    )

                elif line[0] == "c_std:":
                    c_std_flag = line[1]

                elif line[0] == "index_files:":
                    rsp_indx_files = line[1:]

                else:
                    err_msg = (
                        "ERROR: invalid first token in response file: %s"
                        % line[0]
                    )
                    raise RuntimeError(err_msg)

        rsp_defs = c_std_flag + " " + rsp_defs
        return rsp_includes, rsp_defs.strip(), rsp_indx_files

    def get_all_inst_names(self, name, data):
        ret = []
        # Any template types to take care of?
        if "inst" in data and data["inst"]:
            for template_param in data["inst"]:
                template_arg_str = ""
                # If we have > 1 template parameter (ie, class<float, double>),
                # we'll join them here
                if isinstance(template_param, list):
                    template_arg_str = ", ".join(template_param)
                # Else we just have 1 (ie, float)
                else:
                    template_arg_str = template_param
                ret.append(name + "<" + template_arg_str + ">")
        else:
            ret = [name]

        return ret

    # Takes a compound namespace like foo::bar and
    # returns the most recent namespace (bar)
    def most_recent_namespace(self, nmspc):
        return nmspc.split("::")[-1]

    def find_typedef_class_data(self, typedef_list):
        # Gives us the class that the typedef is trying to be
        for typedef_object in typedef_list:
            typedef_class_string = typedef_object.decl_type.decl_string
            template_params = ""

            # Check to see if any template parameters exist
            template_params_start = typedef_class_string.find("<")

            # If so, split off the template parameters to prevent a "::" split from breaking apart
            # the arguments inside the template
            if template_params_start:
                typedef_class_string = typedef_object.decl_type.decl_string[
                    :template_params_start
                ]
                template_params = typedef_object.decl_type.decl_string[
                    template_params_start:
                ]
            typedef_class_list = typedef_class_string.split("::")

            # Reassemble class name
            class_name = typedef_class_list.pop() + template_params
            data = self.name_data

            # For each namespace found in list, proceed down through the data
            for namespace in typedef_class_list[1:]:
                data = data.namespace(namespace)
            # See if class name exists before returning it.
            instance_data = data.class_(class_name)
            if instance_data:
                return_list = []
                for b in instance_data.bases:
                    # If the relationship is public, add to pyclass_args
                    if b.access_type == dec.ACCESS_TYPES.PUBLIC:
                        return_list.append(b.related_class)
                return_list.append(instance_data)
                return return_list

    def generate_bindings(
        self,
        yaml_dict,
        pygccxml_data,
        free_fun_name="free_functions",
        curr_nmspc="",
        func_dict={},
        enum_dict={},
    ):
        if "classes" in yaml_dict:
            classes_dict = yaml_dict["classes"]
            # Take this one class at a time
            for class_name, class_data in classes_dict.items():
                include_list = set({class_data["file"]})
                desired_name = ""
                names_to_find = self.get_all_inst_names(class_name, class_data)
                gen_data_for_class = pygccxml_data.classes(
                    lambda c: c.name in names_to_find
                )
                # Desired name added to allow typedef objects to have written name instead of
                # the name of the original class.
                # TODO: typedef class check goes here.
                if not gen_data_for_class:
                    # Before binding, see if the class is bound elsewhere:
                    typedef = self.name_data.typedefs(
                        lambda c: c.name in names_to_find, recursive=True
                    )
                    if typedef:
                        desired_name = typedef[0].name
                        gen_data_for_class = self.find_typedef_class_data(
                            typedef
                        )
                        # But only generate data for the desired object.
                        gen_data_for_class = [gen_data_for_class[-1]]
                self.write_class_data(
                    class_name,
                    gen_data_for_class,
                    self.opts.output_dir,
                    include_list,
                    desired_name,
                    class_data,
                )

        files_to_include = set()
        all_gen_fun_data = list()
        mod_name = (
            self.most_recent_namespace(curr_nmspc)
            if curr_nmspc
            else free_fun_name
        )
        free_funs_dict = {}
        if "functions" in yaml_dict:
            free_funs_dict = yaml_dict["functions"]
            for fun_name, fun_data in free_funs_dict.items():
                py_module_name = self.customizer.get_curr_mod_name(fun_data)
                if py_module_name is None:
                    if self.opts.cns_flag or mod_name == free_fun_name:
                        py_module_name = self.opts.module_name
                    else:
                        py_module_name = mod_name
                self.namespace_web[py_module_name]["files_to_include"].add(fun_data["file"])
                self.namespace_web[py_module_name]["funs_dict"][fun_name] = fun_data
                gen_data_for_fun = pygccxml_data.free_functions(
                    lambda f: f.name == fun_name
                )
                self.namespace_web[py_module_name]["all_gen_fun_data"].extend(gen_data_for_fun)

        all_gen_enum_data = list()
        enums_dict = {}
        if "enums" in yaml_dict:
            enums_dict = yaml_dict["enums"]
            for enum_name, enum_data in enums_dict.items():
                py_module_name = self.customizer.get_curr_mod_name(enum_data)
                if py_module_name is None:
                    if self.opts.cns_flag or mod_name == free_fun_name:
                        py_module_name = self.opts.module_name
                    else:
                        py_module_name = mod_name
                self.namespace_web[py_module_name]["files_to_include"].add(enum_data["file"])
                self.namespace_web[py_module_name]["enums_dict"][enum_name] = enum_data
                gen_data_for_enum = pygccxml_data.enumeration(enum_name)
                self.namespace_web[py_module_name]["all_gen_enum_data"].append(gen_data_for_enum)

        # Now check for namespaces and recurse
        keys_left_to_check = set(yaml_dict.keys()) - {
            "classes",
            "functions",
            "enums",
        }
        for key in keys_left_to_check:
            new_nmspc = self.add_namespace(curr_nmspc, key)
            self.generate_bindings(
                yaml_dict[key],
                pygccxml_data.namespace(key),
                curr_nmspc=new_nmspc,
            )
        if not curr_nmspc:
            # write out all the non class_data for each namespace
            # need to do it all at once here rather than incrementally
            # as previous to handle complex mixed custom/c++ namespaces->modules
            # Write the free functions and enums to the same file
            for nmspc in self.namespace_web:
                self.write_non_class_data(
                                            nmspc if nmspc != self.opts.module_name else free_fun_name,
                                            self.namespace_web[nmspc]["all_gen_fun_data"],
                                            self.namespace_web[nmspc]["all_gen_enum_data"],
                                            self.opts.output_dir,
                                            self.namespace_web[nmspc]["files_to_include"],
                                            fun_yaml=self.namespace_web[nmspc]["funs_dict"],
                                            enum_yaml=self.namespace_web[nmspc]["enums_dict"],
                                        )

    def template_args_to_underscores(self, name):
        if not name:
            return ""

        # First element is thing to replace, second is its substitute
        chars_to_replace = [("<", "_"), (", ", "_"), (">", ""), ("::", "_")]
        for old, new in chars_to_replace:
            name = name.replace(old, new)

        return name if len(name) > 1 else name[0]

    def generate_wrapper_cpp(self, module_info):
        includes_fmt = '#include "%s"\n'
        class_decs_fmt = "template class %s;\n"
        func_ptr_assign_fmt = "auto %s = &%s;\n"
        includes_str = "".join(
            [includes_fmt % fname for fname in module_info["to_include"]]
        )
        class_decs = "".join(
            class_decs_fmt % cname for cname in module_info["class_insts"]
        )
        # Functions are a bit longer
        func_list = list(module_info["func_insts"])

        # Convert the templated class names to variable names
        # For example, foo<float, double> becomes foo_float_double
        var_names = [
            self.template_args_to_underscores(func_name)
            for func_name in func_list
        ]

        func_ptr_assigns = ""
        for var_name, func_name in zip(var_names, func_list):
            var_name = self.template_args_to_underscores(var_name)
            func_ptr_assigns += func_ptr_assign_fmt % (var_name, func_name)

        wrapper_cpp_file = os.path.join(self.opts.output_dir, "wrapper.cpp")
        with open(wrapper_cpp_file, "w") as file:
            file.write(
                self.opts.wrap_header_fmt.format(
                    includes=includes_str,
                    class_decs=class_decs,
                    func_ptr_assigns=func_ptr_assigns,
                )
            )
        maybe_format_file_in_place(wrapper_cpp_file)

    def compile_and_parse_wrapper(self, rsp_includes, rsp_defs):
        # Need the castxml path at this point
        if self.opts.castxml_path is None:
            raise RuntimeError("ERROR: path to castxml executable not set!")

        castxml_config = pygccxml.parser.xml_generator_configuration_t(
            xml_generator_path=self.opts.castxml_path,
            xml_generator="castxml",
            cflags=rsp_defs,
            castxml_epic_version=1,
            include_paths=[self.opts.source_dir, self.opts.output_dir] + rsp_includes,
        )

        # Run CastXML and parse back the resulting XML into a Python Object.
        pygccxml.utils.loggers.cxx_parser.setLevel(logging.CRITICAL)
        pygccxml.declarations.scopedef_t.RECURSIVE_DEFAULT = False
        pygccxml.declarations.scopedef_t.ALLOW_EMPTY_MDECL_WRAPPER = True
        if self.opts.generation_stage == "2":
            total = pygccxml.parser.parse(
                ["wrapper.cpp"],
                castxml_config,
                compilation_mode=pygccxml.parser.COMPILATION_MODE.ALL_AT_ONCE,
            )

            # Total seems to be a single item list, due to ALL_AT_ONCE mode, capture the data from
            # from the first item in the list
            return total[0]

    def parse_and_generate(self):
        """
        Overall function to perform automatic generation of Pybind11 code from a C++ repository
        - Parses the yaml input
        - Generates a summary of the module
        - Writes instantiations and include into "wrapper.hpp", the only input to CastXML
        - Runs CastXML and uses pygccxml to read results into data object
        - Walks through yaml input, writing class/function/namespace data using the generated
          information from pygccxml
        :return: None
        """
        # init the pygccxml stuff
        # Adapted from CPPWG: https://github.com/jmsgrogan/cppwg/blob/265117455ed57eb250643a28ea6029c2bccf3ab3/cppwg/parsers/source_parser.py#L24
        # Source path is the directory with the yaml in it
        self.opts.source_dir = os.path.dirname(self.opts.yaml_path)
        # Module info to be populated by find_module_data
        module_info = {
            "to_include": set(),
            "class_insts": list(),
            "func_insts": list(),
            "out_names": dict(),
            "indx_out": set(),
            "all_auto_bind": list(),
            "all_auto_bind_defs": set(),
            "non_template_classes": list(),
        }
        wrapper_yaml = yaml.safe_load(open(self.opts.yaml_path, "r"))
        # Generate a summary of the module
        self.find_module_data(
            wrapper_yaml,
            module_info,
            "free_functions",
        )
        module_indexes = {self.opts.module_name: {}}
        for name, item in self.data_web.items():
            module_indexes[self.opts.module_name][name] = item
        module_index_file = os.path.join(
            self.opts.output_dir, self.opts.module_name + ".mdx"
        )
        with open(module_index_file, "w") as indx_file:
            json.dump(self.data_web, indx_file)
        # Short circuit: prints list of files to be generated by the run, if it were to continue.
        if self.opts.generation_stage == "1":
            file_name = posixpath.join(
                self.opts.output_dir, "%s.cpp" % self.opts.module_name
            )
            print(
                "%".join([file_name] + list(module_info["out_names"].keys()))
                + ";"
                + "%".join([module_index_file])
            )
            return

        rsp_includes, rsp_defs, rsp_mdx_files = self.clean_flags(
            self.opts.rsp_path
        )
        self.generate_wrapper_cpp(module_info)
        self.name_data = self.compile_and_parse_wrapper(rsp_includes, rsp_defs)

        for mdx_file in rsp_mdx_files:
            if mdx_file:
                self.load_mdx(mdx_file)

        if self.opts.generation_stage == "2":
            # Drop the namespace strings from the class_insts objects. This ensures they can be found in the classes
            # call below.
            classes_to_find = set(
                module_info["non_template_classes"]
                + [x.split("::")[-1] for x in module_info["class_insts"]]
            )
            classes = self.name_data.classes(
                lambda c: c.name in classes_to_find, recursive=True
            )
            bo = get_binding_order(classes)
            sorted_out_names = []
            for c in bo:
                # MDX strings don't have the leading ::
                remote_module = self.check_auto_bind(
                    c.decl_string.lstrip("::"), self.data_web
                )
                # In classes_to_find: part of current module and should have a future file
                if c.name in classes_to_find:
                    future_file_name = self.find_future_file_name(True, c.name)
                    if future_file_name not in sorted_out_names:
                        sorted_out_names.append(future_file_name)
                # Else: check to see if it's found in a linked module
                elif remote_module:
                    module_sig = self.opts.module_init_fun_signature_fmt.format(
                        module=remote_module
                    )
                    module_fwd = self.opts.module_init_fun_forward_fmt.format(
                        module=remote_module
                    )
                    if module_sig not in module_info["all_auto_bind"]:
                        module_info["all_auto_bind"].append(module_sig)
                    module_info["all_auto_bind_defs"].add(module_fwd)
                # Not found, warn them and add class to skiplist.
                else:
                    print(
                        "Warning: Class %s  was not found in current module or any linked module"
                        % c.name
                    )
                    self.skip_list.add(c.name)
            # Add the current module to the end of the list
            module_info["all_auto_bind"].append(
                self.opts.module_init_fun_signature_fmt.format(
                    module=self.opts.module_name
                )
            )
            module_info["modules"] = module_info["out_names"]
            module_info["out_names"] = (
                list(module_info["out_names"].keys() - set(sorted_out_names))
                + sorted_out_names
            )
            module_file = self.write_module_data(
                self.opts.module_name, module_info, self.opts.output_dir
            )
            self.generate_bindings(
                wrapper_yaml, self.name_data
            )

    def check_auto_bind(self, name, data_web):
        found_module = name
        try:
            while not found_module.startswith("apb11"):
                found_module = data_web[found_module]
        except KeyError:
            return ""
        return found_module.replace("apb11_", "")


def main(argv=None):
    arg = ArgParser(config_file_parser_class=YAMLConfigFileParser)

    arg.add(
        "-o",
        "--output",
        action="store",
        dest="output_dir",
        required=False,
        default=os.getcwd(),
    )
    arg.add(
        "-y",
        "--input_yaml",
        action="store",
        dest="yaml_path",
        help="Path to input YAML file of objects to process",
        required=True,
    )
    arg.add(
        "--module_name",
        action="store",
        dest="module_name",
        help="Desired name of the output PyBind11 module",
        required=True,
    )
    arg.add(
        "-g",
        "--castxml-path",
        action="store",
        dest="castxml_path",
        help="Path to castxml",
        required=False,
    )
    arg.add(
        "-cg",
        "--config-path",
        dest="config_dir",
        required=False,
        is_config_file=True,
        help="config file path",
    )
    arg.add(
        "-s",
        "--stage",
        dest="generation_stage",
        required=True,
        choices=["1", "2"],
        action="store",
    )
    arg.add(
        "-rs", "--input_response", required=False, dest="rsp_path", default=""
    )
    arg.add(
        "-pm",
        "--private_members",
        required=False,
        action="store_true",
        dest="pm_flag",
        default=False,
    )
    namespace_mx_arg_group = arg.add_mutually_exclusive_group()
    namespace_mx_arg_group.add(
        "-ins",
        "--ignore_namespace_structure",
        required=False,
        action="store_true",
        dest="ins_flag",
        default=False,
    )

    namespace_mx_arg_group.add(
        "-cns",
        "--custom_namespace_structure",
        required=False,
        action="store_true",
        dest="cns_flag",
        default=False,
    )

    arg.add(
        "-tpm",
        "--top_python_module",
        dest="top_module",
        required=False,
        action="store",
        default=None,
        help="Name of module this python package will be added to"

    )
    # The formatted strings that will write the pybind code are also configurable
    arg.add(
        "--common_cpp_body_fmt", required=False, default=tb.common_cpp_body
    )
    arg.add(
        "--submodule_signature_fmt",
        required=False,
        default=tb.submodule_signature,
    )
    arg.add(
        "--class_info_body_fmt", required=False, default=tb.class_info_body
    )
    arg.add(
        "--init_fun_signature_fmt",
        required=False,
        default=tb.init_fun_signature,
    )
    arg.add(
        "--init_fun_forward_fmt", required=False, default=tb.init_fun_forward
    )
    arg.add(
        "--init_fun_forward_non_void_fmt",
        required=False,
        default=tb.init_fun_forward_non_void,
    )
    arg.add("--cppbody_fmt", type=str, required=False, default=tb.cppbody)
    arg.add(
        "--class_module_cpp_fmt", required=False, default=tb.class_module_cpp
    )
    arg.add(
        "--non_class_module_cpp_fmt",
        required=False,
        default=tb.non_class_module_cpp,
    )
    arg.add(
        "--non_class_module_return_cpp_fmt",
        required=False,
        default=tb.non_class_module_return_cpp,
    )
    arg.add("--member_func_fmt", required=False, default=tb.member_func)
    arg.add("--constructor_fmt", required=False, default=tb.constructor)
    arg.add(
        "--member_func_arg_fmt", required=False, default=tb.member_func_arg
    )
    arg.add(
        "--public_member_var_fmt", required=False, default=tb.public_member_var
    )
    arg.add(
        "--private_member_var_fmt",
        required=False,
        default=tb.private_member_var,
    )
    arg.add(
        "--member_reference_fmt", required=False, default=tb.member_reference
    )
    arg.add(
        "--overload_template_fmt", required=False, default=tb.overload_template
    )
    arg.add("--wrap_header_fmt", required=False, default=tb.wrap_header)
    arg.add("--operator_fmt", required=False, default=tb.operator_template)
    arg.add("--call_operator_fmt", required=False, default=tb.call_template)
    arg.add("--enum_header_fmt", required=False, default=tb.enum_header)
    arg.add("--enum_val_fmt", required=False, default=tb.enum_val)
    arg.add("--tramp_override_fmt", required=False, default=tb.tramp_override)
    arg.add("--trampoline_def_fmt", required=False, default=tb.trampoline_def)
    arg.add(
        "--pybind_overload_macro_args_fmt",
        required=False,
        default=tb.pybind_overload_macro_args,
    )
    arg.add(
        "--copy_constructor_tramp_fmt",
        required=False,
        default=tb.copy_constructor_tramp,
    )
    arg.add(
        "--publicist_using_directives_fmt",
        required=False,
        default=tb.publicist_using_directives,
    )
    arg.add("--publicist_def_fmt", required=False, default=tb.publicist_def)
    arg.add(
        "--module_init_fun_forward_fmt",
        required=False,
        default=tb.module_init_fun_forward,
    )
    arg.add(
        "--module_init_fun_signature_fmt",
        required=False,
        default=tb.module_init_fun_signature,
    )
    arg.add(
        "--submodule_init_fun_forward_fmt",
        required=False,
        default=tb.submodule_init_fun_forward,
    )
    arg.add(
        "--submodule_init_fun_signature_fmt",
        required=False,
        default=tb.submodule_init_fun_signature,
    )
    arg.add(
        "--submodule_init_fun_forward_non_void_fmt",
        required=False,
        default=tb.submodule_init_fun_forward_non_void,
    )
    arg.add(
        "--submodule_init_fun_signature_return_fmt",
        required=False,
        default=tb.submodule_init_fun_signature_return,
    )
    arg.add(
        "--submodule_return",
        required=False,
        default=tb.submodule_return,
    )
    arg.add(
        "--nullptr_arg_val_fmt", required=False, default=tb.nullptr_arg_val,
    )
    arg.add(
        "--arg_val_cast_fmt", required=False, default=tb.arg_val_cast,
    )
    arg.add(

        "--module_local_fmt", required=False, default=tb.module_local,
    )
    arg.add(
        "--declare_parent_module",
        required=False,
        default=tb.parent_module,
    )
    options = arg.parse_args(argv)

    BindingsGenerator(options).parse_and_generate()


if __name__ == "__main__":
    main()
