#!/usr/bin/env python

"""
Using Python to automatically wrap C++ code into Python via PyBind11
"""

from setuptools import setup

setup(
  name="autopybind11",
  version="0.1A",
  author="Kitware Inc.",
  author_email="kitware@kitware.com",
  long_description=__doc__,
  long_description_content_type="text/rst",
  license="BSD 3 Clause",
  packages=["autopybind11"],
  py_modules=["autopybind11"],
  url="https://gitlab.kitware.com/autopybind11/autopybind11",
  install_requires=[
    "castxml",
    "ConfigArgParse",
    "PyYAML >= 5.1",
    "pygccxml",
    "toposort",
    "pycodestyle"
  ]
)
