# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append(os.getcwd())  # noqa

from module1 import module1
from module2 import module2
# Import of test_base_double from additional causes conflict on run
# from additional.additional import nonTemplate


class additionalTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_test_base_double(self):
        tb = module1.adl_test_base_double()
        self.assertIn("mult", dir(tb))
        tb.base_var = 2
        self.assertEqual(6, tb.mult(3))

    def test_nonTemplate(self):
        nt = module1.nonTemplate()
        self.assertIn("div", dir(nt))
        self.assertIn("fdiv", dir(nt))

        self.assertEqual(6, nt.div(12, 2))
        self.assertAlmostEqual(6, nt.fdiv(18, 3))
        self.assertAlmostEqual(5.90322589874267, nt.fdiv(18.3, 3.1))

    def test_module2_stand_alone(self):
        m2_sa = module2.module2_sa_double()
        m2_sa.base_var = 2
        self.assertEqual(6, m2_sa.base_var * 3)

    def test_module2_inherited(self):
        m2_inh = module2.module2_inh()
        # Directly inherits from nonTemplate
        # Test the same functions
        # self.assertIn("div", dir(m2_inh))
        # self.assertIn("fdiv", dir(m2_inh))

        self.assertEqual(6, m2_inh.div(12, 2))
        self.assertAlmostEqual(6, m2_inh.fdiv(18, 3))
        self.assertAlmostEqual(5.90322589874267, m2_inh.fdiv(18.3, 3.1))

if __name__ == '__main__':
    testRunner = unittest.main(__name__, argv=['main'], exit=False)
    if not testRunner.result.wasSuccessful():
        sys.exit(1)

