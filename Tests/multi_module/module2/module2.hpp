/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */
#ifndef MODULE2_HPP
#define MODULE2_HPP
#include "module1.hpp"

template < typename T = double >
class module2_sa
{
public:
  T base_var;
  inline module2_sa() {};
};

class module2_inh : public nonTemplate
{
public:
  module2_inh();
};

#endif
