'''
  Copyright (c) 2016 Wenzel Jakob <wenzel.jakob@epfl.ch>
  All rights reserved. Use of this source code is governed by a
  BSD-style license that can be found in the LICENSE file.
  Copyright (c) 2016 Wenzel Jakob <wenzel.jakob@epfl.ch>, All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Please also refer to the file CONTRIBUTING.md, which clarifies licensing of
  external contributions to this project including patches, pull requests, etc.
'''


import os
import sys
import unittest
sys.path.append(os.getcwd())  # noqa

from vector2_module import Vector2


class OperatorTestSuite(unittest.TestCase):
    def test_basic_operators(self):
        v1 = Vector2(1, 2)
        v2 = Vector2(3, -1)

        # Unary, not in place
        assert (v1).toString() == "[1.000000, 2.000000]"
        assert (v2).toString() == "[3.000000, -1.000000]"

        assert (+v2).toString() == "[3.000000, -1.000000]"
        assert (-v2).toString() == "[-3.000000, 1.000000]"

        # Non unary, not in place
        assert (v1 + v2).toString() == "[4.000000, 1.000000]"
        assert (v1 - v2).toString() == "[-2.000000, 3.000000]"
        assert (v1 * v2).toString() == "[3.000000, -2.000000]"
        assert (v2 / v1).toString() == "[3.000000, -0.500000]"

        assert (v1 + 8).toString() == "[9.000000, 10.000000]"
        assert (v1 - 8).toString() == "[-7.000000, -6.000000]"
        assert (v1 * 8).toString() == "[8.000000, 16.000000]"
        assert (v1 / 8).toString() == "[0.125000, 0.250000]"

        # In place
        v1 += v2 * 2
        assert v1.toString() == "[7.000000, 0.000000]"
        v1 -= v2
        assert v1.toString() == "[4.000000, 1.000000]"
        v1 *= v2
        assert v1.toString() == "[12.000000, -1.000000]"
        v1 /= v2
        assert v1.toString() == "[4.000000, 1.000000]"

        v1 *= 2
        assert v1.toString() == "[8.000000, 2.000000]"
        v1 /= 16
        assert v1.toString() == "[0.500000, 0.125000]"

        # Some tests for __call__
        assert v2() == 2
        assert v2(10) == 12
        assert v2(10, 20) == 32

        # Equality operators
        v1_copy = Vector2(v1.get_x(), v1.get_y())
        # ==
        assert v1 == v1_copy
        self.assertFalse(not (v1 == v1_copy))
        # !=
        assert v1 != v2
        self.assertFalse(not (v1 != v2))


if __name__ == '__main__':
    testRunner = unittest.main(__name__, argv=['main'], exit=False)
    if not testRunner.result.wasSuccessful():
        sys.exit(1)
