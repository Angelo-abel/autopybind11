
cmake_minimum_required(VERSION 3.15)
project(operators CXX)


find_package(AutoPyBind11)
autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})

add_library(vector2 INTERFACE)
target_sources(vector2  INTERFACE  ${CMAKE_CURRENT_SOURCE_DIR}/Vector2.hpp)
target_include_directories(vector2 INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})

autopybind11_add_module("vector2_module"
                       YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                       CONFIG_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/config.yml
                       DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                       LINK_LIBRARIES vector2
                       )
