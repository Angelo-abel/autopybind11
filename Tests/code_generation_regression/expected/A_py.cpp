#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "${input_dir}/sample.hpp"

namespace py = pybind11;
void apb11_smoke_test_A_py_register(py::module& m) {
  static bool called = false;
  if (called) {
    return;
  }
  called = true;
  py::class_<::A> A(m, "A");

  A.def(py::init<>()).def(py::init<::A const&>(), py::arg("arg0"))

      ;
}
