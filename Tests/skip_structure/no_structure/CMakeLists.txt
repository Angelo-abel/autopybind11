add_library(simpleLib simple.cxx)
set_property(TARGET simpleLib PROPERTY POSITION_INDEPENDENT_CODE ON)
autopybind11_add_module("skip_structure" YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/simple_wrap.yml
                        CONFIG_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/config.yml
                        DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                        LINK_LIBRARIES simpleLib)
