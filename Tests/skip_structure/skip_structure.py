# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append(os.getcwd())  # noqa


class exampleTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_no_structure(self):
        # skip_test was told to ignore namespace structure
        # Find it at the top level instead of in the "one" sub-module
        from no_structure import skip_structure

        self.assertEqual(skip_structure.hello(), 10)

    def test_structure(self):
        # "enforce_structure" doesn't skip
        # Find simple in the "one" sub-module
        from structure import enforce_structure

        self.assertEqual(enforce_structure.one.hello(), 10)


if __name__ == "__main__":
    testRunner = unittest.main(__name__, argv=["main"], exit=False)
    if not testRunner.result.wasSuccessful():
        sys.exit(1)
