/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */
#ifndef INNER_HPP
#define INNER_HPP

namespace inner
{
  class inner_class
  {
  };

  inline void inner_fxn(){}

}
#endif