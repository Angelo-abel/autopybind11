#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "inner.hpp"

namespace py = pybind11;
void apb11_gen_only_module_inner_class_py_register(py::module& m) {
  static bool called = false;
  if (called) {
    return;
  }
  called = true;
  py::class_<::inner::inner_class> inner_class(m, "inner_class");

  inner_class.def(py::init<>())
      .def(py::init<::inner::inner_class const&>(), py::arg("arg0"))

      ;
}
