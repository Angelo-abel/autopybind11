#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "outer.hpp"

namespace py = pybind11;
void apb11_gen_only_module_outer_class_py_register(py::module& m) {
  static bool called = false;
  if (called) {
    return;
  }
  called = true;
  py::class_<::outer_class> outer_class(m, "outer_class");

  outer_class.def(py::init<>())
      .def(py::init<::outer_class const&>(), py::arg("arg0"))

      ;
}
