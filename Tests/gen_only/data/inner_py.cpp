#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "inner.hpp"

namespace py = pybind11;

py::module apb11_gen_only_module_inner_py_register(py::module& m) {
  py::module inner = m.def_submodule("inner", "");
  inner.def("inner_fxn", static_cast<void (*)()>(&::inner::inner_fxn));
  return inner;
}
