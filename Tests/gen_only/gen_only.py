# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
import unittest


# Taken from smoke_test.py
def whitespace_normalize(s):
    # Naive and ugly, but gets the job done.
    return s.replace(" ", "").replace("\n", "").replace("/", "\\")


def import_fn():
    import gen_only_module as gom


class genOnlyTests(unittest.TestCase):
    def test_file_compare(self):
        for file in [
            "inner_class_py.cpp",
            "inner_py.cpp",
            "outer_class_py.cpp",
        ]:
            with open(file, "r") as f:
                with open("data/" + file, "r") as df:
                    expected = whitespace_normalize(df.read())
                    in_text = whitespace_normalize(f.read())
                    self.assertEqual(expected, in_text)

    def test_no_lib(self):
        # No module means an import error is thrown.
        self.assertRaises(ImportError, import_fn)


if __name__ == "__main__":
    unittest.main()
