import simpleTest
s = simpleTest.simple()
r = s.hello()
print("hello returned " + str(r) + "\n")
if r == 10:
    exit(0)
else:
    exit(-1)
