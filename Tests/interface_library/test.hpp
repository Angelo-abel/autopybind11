/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */
#ifndef TEST_HPP
#define TEST_HPP

template < typename T = double >
class test_base
{
public:
  T base_var;
  inline test_base() {}
  inline T base_adder(const T& val) { return base_var + val; }
};

template < typename T = double >
class test
  : public test_base< T >
{

public:
  T val1_var;
  volatile T val2_var;
  T val3_var;

  inline test() {};
  inline test(const T& val1) { test::val1_var = val1; test::val2_var = 0; }
  inline test(const T& val1, float val2) { test::val1_var = val1; test::val2_var = 0; }
  inline void nop() {}
  inline float adder(float t1, float t2) { return t1 + t2; };
  inline float adder(float t1, int t2 = 0) { return t1 + t2; } ;
  inline T get_priv_var1() { return priv_var1; }
  inline T get_priv_var2() { return priv_var2; }
  inline void set_priv_var1(T const& val) { priv_var1 = val; }
  inline static float summer(float t1, int t2 = 0, int t3 = 14) { return t1 + t2 + t3; }

#ifdef TESTFLAG1
  inline void flag1_set() {}
#else
  inline void flag1_not_set() {}
#endif

#if TESTFLAG2
  inline void flag2_set() {}
#else
  inline void flag2_not_set() {}
#endif


private:
  T priv_var1;
  T priv_var2;
};

#endif
