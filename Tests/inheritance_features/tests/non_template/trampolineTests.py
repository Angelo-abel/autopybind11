# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
import unittest
import sys
import os

from py_classes import *

# Make it so we search where we are running.
sys.path.append(os.getcwd())

import inheritance_module as im
from inheritance_module import nmspc1
from inheritance_module.nmspc1 import nmspc2

# Test that trampolines give us the expected functionality.
# Virtual-ness should be honored by Python for both C++ classes exposed in Python,
# and for Python classes inheriting from C++ classes
class trampolines(unittest.TestCase):
    def test_public_virt_overrides_cpp(self):
        # First call whoami from base reference. Downcasting should ensure that
        # this always prints the lowest implementation in the inheritance tree
        # Note that InheritsAll should always match derived2's output.
        # Method isn't implemented in both abstract classes, so this should raise an exception
        self.assertRaises(RuntimeError, im.Base().whoami)
        self.assertRaises(RuntimeError, nmspc1.AbstractDerived1().whoami)

        # Now the concrete classes
        self.assertEqual(im.call_virt_whoami(nmspc1.Derived1()), "Derived1")
        self.assertEqual(im.call_virt_whoami(nmspc2.Derived2()), "Derived2")
        self.assertEqual(im.call_virt_whoami(nmspc2.InheritsAll()), "Derived2")

    def test_prot_virt_overrides_cpp(self):
        # Pure virtual can't be called
        self.assertRaises(RuntimeError, im.Base().call_prot_virt_fxn)
        self.assertRaises(
            RuntimeError, nmspc1.AbstractDerived1().call_prot_virt_fxn
        )

        # Derived 1 sets the implementation, everyone else inherits
        self.assertEqual(
            nmspc1.Derived1().call_prot_virt_fxn(), "Derived1.prot_virt_fxn()"
        )
        self.assertEqual(
            nmspc2.Derived2().call_prot_virt_fxn(), "Derived1.prot_virt_fxn()"
        )
        self.assertEqual(
            nmspc2.InheritsAll().call_prot_virt_fxn(),
            "Derived1.prot_virt_fxn()",
        )

    def test_priv_virt_overrides_cpp(self):
        # Pure virtual can't be called
        self.assertRaises(RuntimeError, im.Base().call_priv_virt_fxn)
        self.assertRaises(
            RuntimeError, nmspc1.AbstractDerived1().call_priv_virt_fxn
        )

        # Derived 1 sets the implementation, everyone else inherits
        self.assertEqual(
            nmspc1.Derived1().call_priv_virt_fxn(), "Derived1.priv_virt_fxn()"
        )
        self.assertEqual(
            nmspc2.Derived2().call_priv_virt_fxn(), "Derived1.priv_virt_fxn()"
        )
        self.assertEqual(
            nmspc2.InheritsAll().call_priv_virt_fxn(),
            "Derived1.priv_virt_fxn()",
        )

    def test_functions_present(self):
        self.assertIn("call_virt_whoami", dir(im))
        self.assertIn("call_virt_from_derived1", dir(nmspc1))
        self.assertIn("call_virt_from_derived2", dir(nmspc2))

    # Test virtual functions can be overriden in python
    def test_public_virt_overrides(self):
        # First DerivedFromBase
        self.assertEqual(
            im.call_virt_whoami(DerivedFromBase()), "DerivedFromBase"
        )

        # Now DerivedFromAbstractDerived1
        self.assertEqual(
            im.call_virt_whoami(DerivedFromAbstractDerived1()),
            "DerivedFromAbstractDerived1",
        )

        # Now check DerivedFromD1. Check that virtual methods can be accessed
        # from a reference to any of its bases
        exp_string = "DerivedFromD1"
        self.assertEqual(im.call_virt_whoami(DerivedFromD1()), exp_string)

        exp_string += ": DerivedFromD1.virt1()"
        self.assertEqual(
            nmspc1.call_virt_from_derived1(DerivedFromD1()), exp_string
        )

        # Now check DerivedFromD2
        exp_string = "DerivedFromD2"
        self.assertEqual(im.call_virt_whoami(DerivedFromD2()), exp_string)

        exp_string += ": DerivedFromD2.virt1()"
        self.assertEqual(
            nmspc1.call_virt_from_derived1(DerivedFromD2()), exp_string
        )

        exp_string += "DerivedFromD2.virt2()"
        self.assertEqual(
            nmspc2.call_virt_from_derived2(DerivedFromD2()),
            exp_string,
        )

        # Now DerivedFromIA. Check that virtual methods are still overriden
        exp_string = "DerivedFromIA"
        self.assertEqual(im.call_virt_whoami(DerivedFromIA()), exp_string)

        exp_string += ": DerivedFromIA.virt1()"
        self.assertEqual(
            nmspc1.call_virt_from_derived1(DerivedFromIA()), exp_string
        )

        exp_string += "DerivedFromIA.virt2()"
        self.assertEqual(
            nmspc2.call_virt_from_derived2(DerivedFromIA()),
            exp_string,
        )

    def test_prot_virt_overrides(self):
        # First classes derived from C++ abstract classes
        exp_string = "DerivedFromBase.prot_virt_fxn()"
        self.assertEqual(DerivedFromBase().call_prot_virt_fxn(), exp_string)

        exp_string = "DerivedFromAbstractDerived1.prot_virt_fxn()"
        self.assertEqual(
            DerivedFromAbstractDerived1().call_prot_virt_fxn(), exp_string
        )

        # Now classes derived from concrete C++ classes
        exp_string = "DerivedFromD1.prot_virt_fxn()"
        self.assertEqual(DerivedFromD1().call_prot_virt_fxn(), exp_string)

        exp_string = "DerivedFromD2.prot_virt_fxn()"
        self.assertEqual(DerivedFromD2().call_prot_virt_fxn(), exp_string)

        exp_string = "DerivedFromIA.prot_virt_fxn()"
        self.assertEqual(DerivedFromIA().call_prot_virt_fxn(), exp_string)

    def test_priv_virt_overrides(self):
        # First DerivedFromBase, which is one of 2 that should work
        exp_string = "DerivedFromBase.priv_virt_fxn()"
        self.assertEqual(DerivedFromBase().call_priv_virt_fxn(), exp_string)

        # Now DerivedFromAbstractDerived1, which should also work
        exp_string = "DerivedFromAbstractDerived1.priv_virt_fxn()"
        self.assertEqual(
            DerivedFromAbstractDerived1().call_priv_virt_fxn(), exp_string
        )

        # The rest of these will currently fail. Private virtual functions with an implementation
        # aren't allowed in the trampoline, so python subclasses should not be able to override them.
        # They will instead use the most recent implementation in the derivation tree on the C++ side
        # DFD1 implements the function, but C++ fails to recognize this new implementation
        exp_string = "Derived1.priv_virt_fxn()"
        self.assertEqual(DerivedFromD1().call_priv_virt_fxn(), exp_string)
        # Likewise with the rest
        self.assertEqual(DerivedFromD2().call_priv_virt_fxn(), exp_string)
        self.assertEqual(DerivedFromIA().call_priv_virt_fxn(), exp_string)


if __name__ == "__main__":
    unittest.main()
