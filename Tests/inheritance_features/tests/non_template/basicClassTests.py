# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import unittest
import sys
import os

# Make it so we search where we are running.
sys.path.append(os.getcwd())

import inheritance_module as im
from inheritance_module import nmspc1
from inheritance_module.nmspc1 import nmspc2

# Test that we can construct classes, set variables, etc...
class basicClassBehavior(unittest.TestCase):
    # This will fail if the classes aren't bound in the correct order.
    # InheritsAll inherits from Derived2
    # Derived2 inherits from Derived1,
    # Derived1 inherits from Base
    def test_order_correct(self):
        # First check Base is bound first
        nmspc1.Derived1()

        # Now check that Derived1 is bound before Derived2
        nmspc2.Derived2()

        # Now check that Derived2 is bound before InheritsAll
        nmspc2.InheritsAll()

    def test_construct(self):
        # First Derived1
        self.assertEqual(nmspc1.Derived1().var1, 0)

        # Now Derived2
        self.assertEqual(nmspc2.Derived2().var1, 0)
        self.assertEqual(nmspc2.Derived2().var2, 0)

        # Now InheritsAll
        self.assertEqual(nmspc2.InheritsAll().var1, 0)
        self.assertEqual(nmspc2.InheritsAll().var2, 0)
        self.assertEqual(nmspc2.InheritsAll().var3, 0)

    def test_get_set_vars(self):
        # First Derived1
        inst = nmspc1.Derived1()
        inst.var1 = 1
        self.assertEqual(inst.var1, 1)

        # Now Derived2
        inst = nmspc2.Derived2()
        inst.var1 = 1
        inst.var2 = 2
        self.assertEqual(inst.var1, 1)
        self.assertEqual(inst.var2, 2)

        # Now InheritsAll
        inst = nmspc2.InheritsAll()
        inst.var1 = 1
        inst.var2 = 2
        inst.var3 = 3
        self.assertEqual(inst.var1, 1)
        self.assertEqual(inst.var2, 2)
        self.assertEqual(inst.var3, 3)

    def test_normal_mthd_calls(self):
        # First Derived1
        self.assertEqual(nmspc1.Derived1().whoami(), "Derived1")
        self.assertEqual(nmspc1.Derived1().virt1(6.28), "Derived1.virt1()")

        # Now Derived2
        self.assertEqual(nmspc2.Derived2().whoami(), "Derived2")
        self.assertEqual(
            nmspc2.Derived2().virt2(-6.28, "py_string"), "Derived2.virt2()"
        )

        # Now InheritsAll
        self.assertEqual(
            nmspc2.InheritsAll().nonvirtual(), "InheritsAll.nonvirtual()"
        )

    # Not testing virtual-ness here, just that it was inherited
    # and can be called by the usual means. Notice that the implementation
    # was provided in Derived1
    def test_public_inherited_mthds(self):
        self.assertEqual(nmspc2.Derived2().virt1(6.28), "Derived1.virt1()")

        # Same for InheritsAll. All of the implementations should be the same as
        # in Derived2
        self.assertEqual(nmspc2.InheritsAll().whoami(), "Derived2")
        self.assertEqual(nmspc2.InheritsAll().virt1(6.28), "Derived1.virt1()")
        self.assertEqual(
            nmspc2.InheritsAll().virt2(-6.28, "py_string"), "Derived2.virt2()"
        )

    # Likewise with above, but with a protected member.
    # Should be "public" on the Python side. Implemented in Base class
    def test_prot_inherited_mthds(self):
        exp_string = "Base.prot_fxn()"
        self.assertEqual(im.Base().prot_fxn(), exp_string)
        self.assertEqual(nmspc1.AbstractDerived1().prot_fxn(), exp_string)
        self.assertEqual(nmspc1.Derived1().prot_fxn(), exp_string)
        self.assertEqual(nmspc2.Derived2().prot_fxn(), exp_string)
        self.assertEqual(nmspc2.InheritsAll().prot_fxn(), exp_string)

    # Also not testing virtual-ness here. Making sure that
    # subclasses can be substitute for a base class in a function argument
    # and that the correct behavior is observed
    def test_subclass_as_argument(self):
        # First check D2 and IA inherit virt1 and its implementation
        # and can be called from reference to base
        derived1_exp_string = "Derived1: Derived1.virt1()"
        derived2_exp_string = "Derived2: Derived1.virt1()"
        self.assertEqual(
            nmspc1.call_virt_from_derived1(nmspc1.Derived1()),
            derived1_exp_string,
        )
        self.assertEqual(
            nmspc1.call_virt_from_derived1(nmspc2.Derived2()),
            derived2_exp_string,
        )
        self.assertEqual(
            nmspc1.call_virt_from_derived1(nmspc2.InheritsAll()),
            nmspc1.call_virt_from_derived1(nmspc2.Derived2()),
        )

        # Now check same thing for virt2 in D2 and IA
        derived2_exp_string = "Derived2: Derived1.virt1()Derived2.virt2()"
        self.assertEqual(
            nmspc2.call_virt_from_derived2(nmspc2.Derived2()),
            derived2_exp_string,
        )
        self.assertEqual(
            nmspc2.call_virt_from_derived2(nmspc2.InheritsAll()),
            nmspc2.call_virt_from_derived2(nmspc2.Derived2()),
        )

    def test_inheritance_relationships(self):
        self.assertTrue(issubclass(nmspc2.InheritsAll, nmspc2.Derived2))
        self.assertTrue(issubclass(nmspc2.InheritsAll, nmspc1.Derived1))
        self.assertTrue(issubclass(nmspc2.InheritsAll, im.Base))

        self.assertTrue(issubclass(nmspc2.Derived2, nmspc1.Derived1))
        self.assertTrue(issubclass(nmspc2.Derived2, im.Base))

        self.assertTrue(issubclass(nmspc1.Derived1, im.Base))


if __name__ == "__main__":
    unittest.main()
