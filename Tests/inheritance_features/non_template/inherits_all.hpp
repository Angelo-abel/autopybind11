/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#ifndef INHERITS_ALL_HPP
#define INHERITS_ALL_HPP

#include "derived2.hpp"
#include <string>

namespace nmspc1 {
namespace nmspc2 {

// Classes that don't define any new virtual methods
// still need to have trampolines + be overridable.
// This class will help test that
class InheritsAll : public Derived2
{
public:
  int var3;
  inline InheritsAll() { var3 = 0; }
  inline std::string nonvirtual()
  {
    return std::string("InheritsAll.nonvirtual()");
  }
};
}}
#endif