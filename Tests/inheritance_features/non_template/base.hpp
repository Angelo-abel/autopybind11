/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#ifndef BASE_HPP
#define BASE_HPP

#include <string>

class Base
{
public:
  float var0;
  virtual std::string whoami() const = 0;

  // Use this to actually call the protected virtual function
  // Can't use an external function since the method is protected
  inline std::string call_prot_virt_fxn() const
  {
    return this -> prot_virt_fxn();
  }

  // Use this to actually call the private virtual function
  // Can't use an external function since the method is private
  std::string call_priv_virt_fxn() const
  {
    return this -> priv_virt_fxn();
  }

protected:
  virtual std::string prot_virt_fxn() const = 0;
  inline std::string prot_fxn() const
  {
    return std::string("Base.prot_fxn()");
  }

private:
  // Make sure private pure virtual functions are included in trampoline.
  // The way that trampolines work actually requires these be overriden,
  // in the trampoline, otherwise the trampoline won't be constructable
  // and attempting to compile will throw an error
  virtual std::string priv_virt_fxn() const = 0;
};

// Calls the virtual functions defined in Base
inline std::string call_virt_whoami(const Base& b)
{
  return b.whoami();
}
#endif