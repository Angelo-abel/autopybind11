/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#ifndef ABSTRACT_DERIVED1_HPP
#define ABSTRACT_DERIVED1_HPP

#include "base.hpp"

namespace nmspc1 {

// This class is only really going to be used
// to test the behavior of pure virtual functions
// that are transparently inherited
class AbstractDerived1 : public Base {};
}
#endif