# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
cmake_minimum_required(VERSION 3.15)
project(inheritance_features CXX)

find_package(AutoPyBind11)
autopybind11_fetch_build_pybind11(PYBIND11_DIR ${PYBIND11_SRC_DIR})

add_library(inheritance INTERFACE)
target_sources(inheritance  INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}/non_template/base.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/non_template/abstract_derived1.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/non_template/derived1.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/non_template/derived2.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/non_template/inherits_all.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/template/tmplt_abstract_derived1.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/template/tmplt_derived1.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/template/tmplt_derived2.hpp)

target_include_directories(inheritance INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})

autopybind11_add_module("inheritance_module"
                       YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                       CONFIG_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/config.yml
                       DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                       LINK_LIBRARIES inheritance
                       )
