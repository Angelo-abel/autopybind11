/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */

#include "blacklist.h"
#include <iostream>
std::tuple<int, std::string> simple::SimpleClass::get_vals()
{
    return std::make_tuple(this->internal_int, this->internal_str);
}

int simple::SimpleClass::get_int_val()
{
    return this->internal_int;
}

std::string simple::SimpleClass::get_str_val()
{
    return this->internal_str;
}

void simple::SimpleClass::set_int_val(int a)
{
    this->internal_int = a;
}

void simple::SimpleClass::set_str_val(std::string str)
{
    this->internal_str = str;
}

void simple::SimpleClass::print_internals()
{
    std::cout<<"Internal int: "<<this->internal_int<<std::endl;
    std::cout<<"Internal str: "<<this->internal_str<<std::endl;
    return;
}

void simple::SimpleClass::wrong_one(int t)
{
  std::cout << "Not this one";
}

void simple::SimpleClass::found()
{
  std::cout << "This one's good";
}

void simple::SimpleClass::notFound()
{
  std::cout << "Not this one either";
}

void simple::SimpleClass::im_protected_and_excluded()
{
  std::cout<<"I'm protected and excluded"<< std::endl;
}