/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt      */
#include <notmylib.h>

class better_printer
{
public:
    better_printer() : other_printer(printer()) {};
    better_printer(std::string message) : other_printer(printer(message)) {};
    void better_print() {std::cout << "Message: "; other_printer.print();};
private:
    printer other_printer;
};