/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */
#ifndef FIRST_NAMESPACE_HPP
#define FIRST_NAMESPACE_HPP
namespace first
{
  template < typename S=float, typename T=double >
  class nmspc1_class
  {
  public:
    S var1;
    T var2;

    inline nmspc1_class()
    {
      var1 = S( 0 );
      var2 = T( 0 );
    }

    inline void update_vars( S update1, T update2 )
    {
      var1 += update1;
      var2 += update2;
    }

    inline void update_vars( S update )
    {
      var1 += update;
      var2 += T( update );
    }
  };

  template < typename S, typename T >
  T nmspc1_func( S val1, T val2 )
  {
    return T( val1 + val2 );
  }
}
#endif